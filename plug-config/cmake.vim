nmap <F5> :CMake<CR>
nmap <silent> <F6> :wall!<CR>:AsyncRun make -n<CR>
nmap <silent> <F7> :AsyncRun -raw make run<CR>
nmap <silent> <F9> :AsyncRun make clean<CR>

noremap <F8> :call asyncrun#quickfix_toggle(10)<cr>

let g:cmake_build_type="DEBUG"
let g:cmake_export_compile_commands=1

au FileType qf setlocal nonumber colorcolumn=

command! -bang -nargs=* -complete=file Make AsyncRun -program=make @ <args>


