if vim.fn.exists('g:vscode') == 0 then
  ------------------------------------HELPERS-------------------------------------
  local cmd = vim.cmd
  local fn = vim.fn
  local g = vim.g
  local opt = vim.opt

  local function map(mode, lhs, rhs, opts)
    local options = {noremap = true}
    if opts then options = vim.tbl_extend('force', options, opts) end
    vim.api.nvim_set_keymap(mode, lhs, rhs, options)
  end

  -------------------------------------PLUGINS------------------------------------
  cmd 'packadd paq-nvim'
  local paq = require('paq-nvim').paq
  paq {'savq/paq-nvim', opt = true}
  paq {'nvim-lua/completion-nvim'}
  paq {'nvim-treesitter/nvim-treesitter'}
  paq {'neovim/nvim-lspconfig'}
  paq {'junegunn/fzf', run = fn['fzf#install']}
  paq {'junegunn/fzf.vim'}
  paq {'morhetz/gruvbox'}
  paq {'jiangmiao/auto-pairs'}

  -------------------------------------OPTIONS------------------------------------
  cmd 'colorscheme gruvbox'
  opt.completeopt = {'menuone', 'noinsert', 'noselect'}
  opt.expandtab = true
  opt.hidden = true
  opt.ignorecase = true
  opt.joinspaces = true
  opt.list = true
  opt.number = true
  opt.relativenumber = true
  opt.scrolloff = 4
  opt.shiftround = true
  opt.shiftwidth = 2
  opt.sidescrolloff = 8
  opt.smartcase = true
  opt.smartindent = true
  opt.splitbelow = true
  opt.splitright = true
  opt.tabstop = 2
  opt.termguicolors = true
  opt.wildmode = {'list', 'longest'}
  opt.wrap = false
  cmd 'set mouse=a'
  cmd 'set shortmess+=c'

  -------------------------------------MAPPINGS-----------------------------------
  g.mapleader = ","
  map('', '<leader><leader>', ':b#<CR>')
  map('', '<leader>r', ':Rg<CR>')
  map('', '<leader>f', ':Files<CR>')
  map('', '<leader>c', '"+y')
  map('i', '<C-u>', '<C-g>u<C-u>')
  map('i', '<C-w>', '<C-g>u<C-w>')

  -- <Tab> for completion
  map('i', '<S-Tab>', 'pumvisible() ? "\\<C-p>" : "\\<S-Tab>"', {expr = true})
  map('i', '<Tab>', 'pumvisible() ? "\\<C-n>" : "\\<Tab>"', {expr = true})

  map('n', '<C-l>', '<cmd>noh<CR>')
  map('n', '<leader>o', 'm`o<Esc>``')

  ------------------------------------TREESITTER----------------------------------
  local ts = require 'nvim-treesitter.configs'
  ts.setup {ensure_installed = 'maintained', highlight = {enable = true}}

  ------------------------------------LSP-----------------------------------------
  local lsp = require 'lspconfig'
  local completion = require 'completion'

  lsp.clangd.setup {
      on_attach = completion.on_attach,
      default_config = {
          cmd = {
              "clangd", "--background-index", "--clang-tidy", "--suggest-missing-includes"
          },
          filetypes = {"c", "cpp"},
          root_dir = lsp.util.root_pattern("build/compile_commands.json", "compile_commands.json")
      }
  }

  lsp.pylsp.setup{on_attach = completion.on_attach}

  lsp.rust_analyzer.setup{on_attach = completion.on_attach}

  lsp.hdl_checker.setup{}

  map('n', '<space>,', '<cmd>lua vim.lsp.diagnostic.goto_prev()<CR>')
  map('n', '<space>;', '<cmd>lua vim.lsp.diagnostic.goto_next()<CR>')
  map('n', '<space>a', '<cmd>lua vim.lsp.buf.code_action()<CR>')
  map('n', '<space>d', '<cmd>lua vim.lsp.buf.definition()<CR>')
  map('n', '<space>f', '<cmd>lua vim.lsp.buf.formatting()<CR>')
  map('n', '<space>h', '<cmd>lua vim.lsp.buf.hover()<CR>')
  map('n', '<space>m', '<cmd>lua vim.lsp.buf.rename()<CR>')
  map('n', '<space>r', '<cmd>lua vim.lsp.buf.references()<CR>')
  map('n', '<space>s', '<cmd>lua vim.lsp.buf.document_symbol()<CR>')

  ------------------------------------COMMANDS------------------------------------
  cmd 'au TextYankPost * lua vim.highlight.on_yank {on_visual = false}'
end
