call plug#begin(stdpath('data') . 'plugged')
" Colorscheme
Plug 'morhetz/gruvbox'

" Completion
Plug 'neovim/nvim-lspconfig'
Plug 'nvim-lua/completion-nvim'
Plug 'nvim-treesitter/nvim-treesitter', {'do': ':TSUpdate'}  " We recommend updating the parsers on update

" Status bar
Plug 'vim-airline/vim-airline'
Plug 'vim-airline/vim-airline-themes'

" Search
Plug 'junegunn/fzf', { 'do': { -> fzf#install() } }
Plug 'junegunn/fzf.vim'
Plug 'airblade/vim-rooter'
Plug 'tpope/vim-fugitive'
Plug 'airblade/vim-gitgutter'
Plug 'preservim/nerdtree'

" Tools
Plug 'skywind3000/asyncrun.vim'
Plug 'vhdirk/vim-cmake'

call plug#end()

source ~/.config/nvim/plug-config/config.vim
source ~/.config/nvim/plug-config/nerdtree.vim
source ~/.config/nvim/plug-config/lsp.vim
source ~/.config/nvim/plug-config/airline.vim
source ~/.config/nvim/plug-config/fzf.vim
source ~/.config/nvim/plug-config/cmake.vim
